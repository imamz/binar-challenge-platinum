import React from "react";
import DataDashboard from "../Component/DataDashboard";
import NavigationBar from "../Component/NavigationBar";
import NavigationHeader from "../Component/NavigationHeader";
import { useLocation } from "react-router-dom";

const Dashboard = () => {
  const location = useLocation();
  const data = location.state;
  return (
    <div className="d-flex">
      <NavigationBar />
      <div className="w-100">
        <NavigationHeader />
        <DataDashboard props={data} />
      </div>
    </div>
  );
};
export default Dashboard;
