import React, { useState } from "react";

const NavigationBar = () => {
  return (
      <div className="py-1 sidebar d-flex flex-column align-items-center" >
        <div className="logo-admin"></div>
        <img className="sidebar-item" src="./Assets/Home.png" alt=" " />
        <img className="sidebar-item sidebar-active" src="./Assets/Administrator.png" alt=" " />
      </div>
  );
};
export default NavigationBar;
