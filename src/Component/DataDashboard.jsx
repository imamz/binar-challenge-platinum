import React, { useEffect, useState, PureComponent } from "react";
import { useSelector, useDispatch } from "react-redux";
import { BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import axios from "axios";

const DataDashboard = (props) => {

const dataA = [
  {
    name: 'Page A',
    uv: 4000,
    pv: 2400,
    amt: 2400,
  },
  {
    name: 'Page B',
    uv: 3000,
    pv: 1398,
    amt: 2210,
  },
  {
    name: 'Page C',
    uv: 2000,
    pv: 9800,
    amt: 2290,
  },
  {
    name: 'Page D',
    uv: 2780,
    pv: 3908,
    amt: 2000,
  },
  {
    name: 'Page E',
    uv: 1890,
    pv: 4800,
    amt: 2181,
  },
  {
    name: 'Page F',
    uv: 2390,
    pv: 3800,
    amt: 2500,
  },
  {
    name: 'Page G',
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
  {
    name: 'Page H',
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
  {
    name: 'Page I',
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
  {
    name: 'Page J',
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
  {
    name: 'Page K',
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
  {
    name: 'Page L',
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
  {
    name: 'Page M',
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
  {
    name: 'Page N',
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
  {
    name: 'Page O',
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
  {
    name: 'Page P',
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
  {
    name: 'Page Q',
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
  {
    name: 'Page R',
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
  {
    name: 'Page S',
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
  {
    name: 'Page T',
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
  {
    name: 'Page U',
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
  {
    name: 'Page V',
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
  {
    name: 'Page W',
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
  {
    name: 'Page X',
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
  {
    name: 'Page Y',
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
  {
    name: 'Page Z',
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
  {
    name: 'Page AA',
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
  {
    name: 'Page BB',
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
  {
    name: 'Page CC',
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
  {
    name: 'Page DD',
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
];


const columns = [{
  dataField: 'pv',
  text: 'pv',
  sort: true

}, {
  dataField: 'name',
  text: 'Product Name',
  sort: true
}, {
  dataField: 'uv',
  text: 'Product Price',
  sort: true
}];

    console.log(props.props);

    return (
    <div className="container-fluid d-flex ps-0" style={{ backgroundColor: "#E5E5E5", height: "100%" }}>
        <div className="menu flex-1" >
            <h5 className="menu-list" style={{ color: "grey"}}>DASHBOARD</h5>
            <h5 className="menu-item">Dashboard</h5>
        </div>
        <div className="content">
            <div className="row">
                <div className="col pb-1">
                    <div className="breadcrumbs d-flex mt-4 pt-2">
                        <h5>Dashboard</h5>
                        <span>&nbsp; > &nbsp;</span>
                        <p className="mb-2">Dashboard</p>
                    </div>
                    <div className="row mb-4">
                      <div className="title-page d-flex mb-2">
                        <div className="rectangle-mini me-2"></div>
                        <h5 className="mb-2">Rented Car Data Visualization</h5>
                      </div>
                      
                    </div>
                </div>
            </div>
        
            <div className="data-chart" style={{ width: "100%"}}>
              <form action="#" className="mb-4">
                <p className="mb-1">Month</p>
                <div className="d-flex">
                  <select
                    type="select"
                    name="kategori"
                    style={{ width: "150px" }}
                    className="border border-2 form-control"
                    >
                    <option value="maret-2023">maret-2023</option>
                    <option value="medium">Medium</option>
                    <option value="large">Large</option>
                  </select>
                  <button type="submit" className="btn btn-primary">Go</button>
                </div>
              </form>
                {/* <ResponsiveContainer width="100%"> */}
                    <BarChart width={1050} height={400} data={dataA}>
                        <XAxis dataKey="name" scale="point" padding={{left: 10, right: 10}} />\
                        <YAxis/>
                        <Tooltip/>
                        <Legend/>
                        <CartesianGrid strokeDasharray="3 3" />
                        <Bar dataKey="uv" fill="#8884d8" />
                    </BarChart>
                {/* </ResponsiveContainer> */}
            </div>

            <div className="data-tabel mt-5r.">
              <h2>Dashboard</h2>
              <div className="title-page d-flex mb-2">
                <div className="rectangle-mini me-2"></div>
                <h5 className="mb-2">Data List</h5>
              </div>
              <BootstrapTable keyField='id' data={ dataA } columns={ columns } pagination={ paginationFactory() } headerClasses="header-class" />
            </div>
        </div>
    </div>
    );
};

export default DataDashboard;
